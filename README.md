# 2TP - STS VuePress

A sample project to let you see what makes VuePress so great!

## Getting Setup

### Requirements

1.  [Node.js](https://nodejs.org/en/) v8.0+

### Instructions

1.  Clone the repo
2.  Run `yarn`
3.  Run `yarn dev`

## Contributing

This site is built on [VuePress](https://vuepress.vuejs.org/). Please see their [Guide](https://vuepress.vuejs.org/guide/) for more information on how it works.

## Deployment

[![Deploy to Netlify](https://www.netlify.com/img/deploy/button.svg)](https://app.netlify.com/start/deploy?repository=https://gitlab.com/wiven/sts-vuepress.git)
